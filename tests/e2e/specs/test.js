module.exports = {
  'add todo acceptance test': browser => {

    console.log(process.env.VUE_APP)
    browser
        .url(process.env.VUE_APP_BASE_URL)
        .waitForElementVisible('#app')
        .waitForElementVisible('#todoInput', 5000)
        .waitForElementVisible('#addTodoButton', 5000)

        .setValue("#app #todoInput", 'buy some milk')
        .click('#addTodoButton')

        .waitForElementVisible('#todoList', 5000)
        .assert.containsText("li:last-child", "buy some milk")
        .end();

  }
}

import {createLocalVue, shallowMount} from '@vue/test-utils'
import AddTodo from "../../src/components/AddTodo";
import Vuex from 'vuex'
import store from "../../src/store";
import todoClient from "../../src/client/todoClient";

const localVue = createLocalVue();

localVue.use(Vuex);

describe('AddTodo.vue', () => {
  let wrapper = null;

  beforeEach(() => {
    wrapper = shallowMount(AddTodo, { store, localVue });
  });

  it('renders add todo component', () => {
    const todoInput = wrapper.find("#todoInput");
    const addTodoButton = wrapper.find("#addTodoButton");

    expect(todoInput.exists()).toBe(true);
    expect(addTodoButton.exists()).toBe(true);
  });

  it('add todo', async () => {
    const todoInput = wrapper.find("#todoInput");
    todoInput.setValue("buy some milk");
    const addTodoButton = wrapper.find("#addTodoButton");
    todoClient.addNewTodo = jest.fn().mockImplementationOnce(() => Promise.resolve())

    addTodoButton.trigger('click');

    await wrapper.vm.$nextTick()
    expect(todoClient.addNewTodo).toHaveBeenCalledTimes(1)
  });


})

import {createLocalVue, shallowMount} from '@vue/test-utils'
import Vuex from 'vuex'
import TodoList from "../../src/components/TodoList";

const localVue = createLocalVue();

localVue.use(Vuex);

describe('TodoList.vue', () => {
    let wrapper = null;

    beforeEach(() => {
        const todoItems = [
            { title: "foo" },
            { title: "bar" },
        ];

        const store = new Vuex.Store({
            getters: {
                getTodoItems: () => todoItems
            },
            actions : {
                addNewTodoItem: jest.fn(),
                getAllTodoItems: jest.fn()
            }
        });

        wrapper = shallowMount(TodoList, { store, localVue });
    })

    it('renders todo list component', () => {
        const todoInput = wrapper.find("#todoList");
        expect(todoInput.exists()).toBe(true);
    })

   it('should todo list size two', () => {
        const todos = wrapper.findAll("li");

        expect(todos.exists()).toBe(true);
        expect(todos.length).toBe(2);
    })
})

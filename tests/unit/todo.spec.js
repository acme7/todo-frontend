import {shallowMount} from '@vue/test-utils'
import Todo from "../../src/components/Todo";


describe('Todo.vue', () => {

  it('renders todo component', () => {
    const wrapper = shallowMount(Todo, {
      propsData: {
        item: {
          title: "hi"
        }
      }
    });
    const todoItem = wrapper.find(".todoItem");

    expect(todoItem.exists()).toBe(true);
  });


})

import todoClient from "../../../src/client/todoClient";
import axios from "axios";

const {pactWith} = require("jest-pact")

pactWith({port: 8080, cors: true, consumer: "TodoWebConsumer", provider: "TodoWebProvider"}, provider => {
    axios.defaults.baseURL = "http://127.0.0.1:8080";

    describe("Todos Test", () => {
        const TODO_LIST = [
            {
                title: "buy some milk"
            }
        ]

        describe("when doing a call to fetch todos", () => {
            beforeEach(() => {
                const interaction = {
                    state: "i have a list of todos",
                    uponReceiving: "request to retrieve all todos",
                    withRequest: {
                        method: "GET",
                        path: "/todos",
                        headers: {
                            "Accept": "application/json",
                        },
                    },
                    willRespondWith: {
                        status: 200,
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: TODO_LIST,
                    },
                };
                return provider.addInteraction(interaction)
            });

            it("should fetch the list of todos", async () => {
                let response = await todoClient.getTodoItems();
                expect(response).toEqual(TODO_LIST);
            })
        })

        describe("add new todo", () => {
            beforeEach(() => {
                const interaction = {
                    state: "adding a new todo",
                    uponReceiving: "created answer needs to return",
                    withRequest: {
                        method: "PUT",
                        path: "/todos",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: TODO_LIST[0]
                    },
                    willRespondWith: {
                        status: 201,
                    },
                };

                return provider.addInteraction(interaction)
            });

            it("should return created", async () => {

                let response = await todoClient.addNewTodo(TODO_LIST[0]);
                expect(response).toEqual(201);
            })
        })
    });
});
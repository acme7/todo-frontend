# Todo Frontend Application

This is a simple todo app.

## Tech Stack

 - VueJs
 - Axios
 - Jest
 - PactJS
 - NightwacthJS

## API Architecture

* Development application works here `http://161.35.254.147:8080`
* Production application works here `http://164.90.255.115:8080`


## Testing 

  ```
  npm run test:unit
  npm run test:contract
  npm run test:e2e
  ```
  
## Run

```
 npm run serve
```





import Vue from 'vue'
import App from './App.vue'
import store from "./store";

Vue.config.productionTip = false
axios.defaults.baseURL = process.env.VUE_APP_SERVER_URL

new Vue({
  render: function (h) { return h(App) },
  store,
}).$mount('#app')

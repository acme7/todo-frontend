import axios from "axios"

export default {

    async addNewTodo(todoItem) {
        const json = JSON.stringify(todoItem);
        const response = await axios.put("/todos", json, {
            headers: { "Content-Type": "application/json"
            }
        });

        return response.status;
    },

    async getTodoItems() {
        const response = await axios.get("/todos", {
            headers: { Accept: "application/json"
            }
        });

        return response.data;
    },

}

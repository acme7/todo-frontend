import Vue from 'vue';
import Vuex from 'vuex';
import todoClient from "../client/todoClient";

window.axios = require('axios');

Vue.use(Vuex);

const state = {
    todoItems : []
};

const getters = {
    getTodoItems(state) {
        return state.todoItems;
    }
};

const mutations = {
    addNewTodoItem(state, todoItem) {
        const item = {
            title : todoItem
        };

        state.todoItems.push(item.title);
    },
    setTodoItems(state, todoItems) {
        state.todoItems = todoItems;
    }
};

const actions = {
    addNewTodoItem({ commit }, todoItem) {
        todoClient.addNewTodo(todoItem).then(() => {
            commit('addNewTodoItem', todoItem);
        }).catch(error => {
            console.log("addNewTodoItem : " , error);
        });
    },
    getAllTodoItems({ commit }) {
        todoClient.getTodoItems().then(data => {
           commit('setTodoItems', data);
        }).catch(error => {
            console.log("getAllTodoItems : " , error);
        });
    }
};

const store = new Vuex.Store({
   state,
   getters,
   mutations,
   actions
});

export default store;
